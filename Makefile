ifeq ($(PREFIX),)
    PREFIX := /usr
endif


all:
	echo "Nothing to do in build step"

install:
	install -d $(DESTDIR)$(PREFIX)/bin/
	install src/mswinurl-handler $(DESTDIR)$(PREFIX)/bin/
	install -d $(DESTDIR)$(PREFIX)/share/applications
	install -m644 src/mswinurl-handler.desktop $(DESTDIR)$(PREFIX)/share/applications