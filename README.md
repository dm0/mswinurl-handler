# Open Windows and macOS `.url` files on Linux

This tiny package adds support for opening `.url` files on Linux.

The URL stored in the `.url` file is opened using default browser (via `xdg-open`).


## Installation

Download and install `.deb` package from [Releases](/../releases)


## URL files

Files with extension `.url` are widely used in Windows ans macOS operation 
systems to store an URL in a way that allows opening URL by simply activating 
the file (e.g. via double click).

The `.url` files are actually text files:
```ini
[InternetShortcut]
URL=http://www.example.com
```

Mime type of `.url` files is `application/x-mswinurl`.


## Package details

The package installs only two files: 
* A script that finds `URL=` line in the `.url` file and executes `xdg-open` 
  with the URL.
* Desktop entry file (`.desktop`) that registers script to open 
  `application/x-mswinurl` files.

Actual mime handler registration process is performed by the Debian package 
manager during the package installation.


## Credits

This work in inspired by the excellent article in Daniel Brice's blog: 
http://www.danielbrice.net/blog/opening-url-file-like-a-pro/

Project icon: https://www.iconfinder.com/icons/65946/url_icon

