function signature( author, date ) {
    if(date) { print "\n -- " author "  "date "\n\n"}
}

/^\s*$/{ next }
/^##[ ]+[0-9]+-[0-9]+-[0-9]+/{
    signature(author, date)
    if(match($0, /v([0-9]+\.)+[0-9]+/)){
        version = substr($0, RSTART + 1, RLENGTH - 1);
    }
    if(match($0, /^(##[ ]+)[0-9]+-[0-9]+-[0-9]+/)){
        date = substr($0, RSTART + 1, RLENGTH - 1);
        gsub(/[# ]/,"",date);
        command = "date -R -d " date
        command | getline date
    }
    print package " (" version ") stable; urgency=medium\n";
}
!/^##[ ]+[0-9]+-[0-9]+-[0-9]+/{
    if(match($0, /^#+[ ]+/)) { $0=substr($0, RLENGTH + 1)}
    if(version) { print "  " $0}
}
END{
    signature(author, date)
}